package com.org.ThirdApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ThirdStep {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:msql://localhost:3306?user=root&password=root*";
		Statement stmt=null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("diriver loaded and registered...");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306?user=root&password=root");
					System.out.println("conntection established");
					stmt=con.createStatement();
					System.out.println("platform created");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(stmt!=null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
