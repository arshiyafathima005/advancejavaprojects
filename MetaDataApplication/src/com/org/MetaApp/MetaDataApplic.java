package com.org.MetaApp;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
public class MetaDataApplic {

	public static void main(String[] args) {
		Connection con=null;
		Statement stmt=null;
		ResultSet rs=null;
		String qry="select * from oejm5.sinfo";
		try {
			Class.forName("com.mysql.jdbc.Driver");
				con=DriverManager.getConnection("jdbc:mysql://localhost:3306?user=root&password=root");
				
				DatabaseMetaData dbmd=con.getMetaData();
				System.out.println("Driver class name"+dbmd.getDriverName());
				System.out.println("Driver class Version"+dbmd.getDriverVersion());
				//System.out.println(dbmd.getDatabaseProductName());
				//System.out.println(dbmd.getDefaultTransactionIsolation());
				stmt=con.createStatement();
				rs=stmt.executeQuery(qry);
				
				ResultSetMetaData rsmd=rs.getMetaData();
				System.out.println("total num of columns --->"+rsmd.getColumnCount());
				System.out.println("Name of 3rd column--->"+rsmd.getColumnLabel(3));
				System.out.println("3rd column display size--->"+rsmd.getColumnDisplaySize(3));
				
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(stmt!=null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(rs!=null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		}

	}


