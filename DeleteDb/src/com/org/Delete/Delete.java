package com.org.Delete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Delete {

	

	public static void main(String[] args) {
		Connection con=null;
		Statement stmt=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root*";
		String deleteqry="delete from oejm5.emp where id=3";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("driver loaded and reg..");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306?user=root&password=root");
			System.out.println("connection established");
			stmt=con.createStatement();
			System.out.println("platform created");
			stmt.executeUpdate(deleteqry);
			System.out.println("data deleted");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(stmt!=null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}