package oar.org.LightApp;

import java.util.Scanner;

public class LightMain {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the name of light");
		String type=sc.next();
		sc.close();
		ISwitch sw=LightFactory.getLight(type);
		if(sw!=null)
		{
			sw.sOn();
			sw.sOff();
		}
	}

}
