package oar.org.LightApp;

public class LightFactory {
public static ISwitch getLight(String type)
{
	if(type.equalsIgnoreCase("tubelight"))
	{
		return new TubeLight();
	}
	else if(type.equalsIgnoreCase("ledlight"))
	{
		return new LedLight();
	}
	else
	{
		System.err.println("light not found");
	}
	return null;
}
}
