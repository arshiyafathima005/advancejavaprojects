package com.org.update;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUpdate {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root*";
		Statement stmt=null;
		String upqry="update oejm5.emp set salary=70000.00 where id=3";
	
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded and registered...");
			
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306?user=root&password=root");
			System.out.println("connection established");
			
			stmt=con.createStatement();
			System.out.println("platform created");
			
			stmt.executeUpdate(upqry);
			System.out.println("db updated");
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(stmt!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		

	}

}
