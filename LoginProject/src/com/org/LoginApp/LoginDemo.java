package com.org.LoginApp;
import java.sql.*;
import java.util.Scanner;
public class LoginDemo {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String qry="select name from oejm5.user where userName=? and password=?";
		Scanner sc=new Scanner(System.in);
		System.out.println("enter username");
		String uname=sc.next();
		System.out.println("enter password");
		String psw=sc.next();
		sc.close();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("dirver loaded and registered...");
			con=DriverManager.getConnection(url);
			System.out.println("connection established");
			pstmt=con.prepareStatement(qry);//compile
			pstmt.setString(1,uname);
			pstmt.setString(2,psw);
			rs=pstmt.executeQuery();//execute
			if(rs.next())
			{
				String name=rs.getString(1);
				System.out.println("welcome "+name+"...");
			}else
			{
			System.out.println("invalid username or password....!!");	
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(rs!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
