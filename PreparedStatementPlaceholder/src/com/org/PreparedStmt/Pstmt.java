package com.org.PreparedStmt;
import java.sql.*;
public class Pstmt {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		PreparedStatement pstmt=null;
		String inqry="insert into oejm5.emp values(?,?,?,?)";
	try {
		Class.forName("com.mysql.jdbc.Driver");
		con=DriverManager.getConnection(url);
	pstmt=con.prepareStatement(inqry);//compile once
	pstmt.setInt(1,7);
	pstmt.setString(2,"riyaz");
	pstmt.setDouble(3,60000.00);
	pstmt.setString(4,"intel");
	pstmt.executeUpdate();//execute +1st record
	System.out.println("1st record is inserted");
	
	pstmt.setInt(1,8);
	pstmt.setString(2,"badrun");
	pstmt.setDouble(3,90000.00);
	pstmt.setString(4,"hp");
	pstmt.executeUpdate();//execute +2nd record
	System.out.println("2nd record is inserted");
	
	pstmt.setInt(1,9);
	pstmt.setString(2,"samed");
	pstmt.setDouble(3,75000.00);
	pstmt.setString(4,"LG");
	pstmt.executeUpdate();//execute 3rd record
	System.out.println("3rd record is inserted");
	
	
	} catch (ClassNotFoundException | SQLException e) {
		e.printStackTrace();
	}
	finally
	{
		if(con!=null)
		{try {
			con.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
		if(pstmt!=null)
		{try {
			con.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	}
	}
}


