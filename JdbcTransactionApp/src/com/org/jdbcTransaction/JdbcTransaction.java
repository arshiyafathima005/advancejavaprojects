package com.org.jdbcTransaction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
public class JdbcTransaction {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		PreparedStatement pstmt1=null;
		PreparedStatement pstmt2=null;
		String qry1="insert into oejm5.account1 values(?,?,?,?)";
		String qry2="insert into oejm5.account2 values(?,?,?)";
		Scanner sc=new Scanner(System.in);
		System.out.println("enter accno:");
		int accno=sc.nextInt();
		System.out.println("enter name:");
		String name=sc.next();
		System.out.println("enter bank name:");
		String bank=sc.next();
		System.out.println("account bal");
		double balance=sc.nextDouble();
		System.out.println("enter branch :");
		String branch=sc.next();
		sc.close();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(url);
			con.setAutoCommit(false);
			pstmt1=con.prepareStatement(qry1);
			pstmt1.setInt(1, accno);
			pstmt1.setString(2,name);
			pstmt1.setString(3,bank);
			pstmt1.setDouble(4, balance);
			pstmt1.executeUpdate();
			System.out.println("account table 1 is inserted");
			
			pstmt2=con.prepareStatement(qry2);
			pstmt2.setInt(1, accno);
			pstmt2.setString(2,name);
			pstmt2.setString(3,branch);
			pstmt2.executeUpdate();
			System.out.println("account table 2 is inserted");
			
			con.commit();
		} catch (ClassNotFoundException | SQLException e)
		{
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt1!=null)
			{
				try {
					pstmt1.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt2!=null)
			{
				try {
					pstmt2.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
