package com.org.BatchUpdateStatement;

import java.sql.*;

public class BatchUpStmt {

	public static void main(String[] args) {
		Connection con=null;
		Statement stmt=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		String inqry="insert into oejm5.emp values(10,'asma',50000,'HDFC')";
		String upqry="update oejm5.emp set salary=85000.00 where salary=75000.00";
		String delqry="delete from oejm5.emp where id=4";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(url);
			stmt=con.createStatement();
			stmt.addBatch(inqry);
			stmt.addBatch(upqry);
			stmt.addBatch(delqry);
			int[] arr=stmt.executeBatch();
			for(int i:arr)
			{
				System.out.println(i);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(stmt!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
