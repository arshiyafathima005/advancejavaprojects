package com.org.BatchUpdateStatement;

import java.sql.*;

public class BatchUpdatePreparedStmt {

	public static void main(String[] args) {
		Connection con=null;
		PreparedStatement pstmt=null;
		PreparedStatement pstmt2=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		String inqry="insert into oejm5.emp values(12,'haleema',35000.00,'International School')";
		String upqry="update oejm5.emp set salary=65000.00 where id=3";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(url);
			pstmt=con.prepareStatement(inqry);
			pstmt.addBatch();
			int[] arr=pstmt.executeBatch();
			for(int i:arr)
			{
				System.out.println(i);
			}
			pstmt2=con.prepareStatement(upqry);
			pstmt2.addBatch();
			int[] arr2=pstmt2.executeBatch();
			for(int j:arr2)
			{
				System.out.println(j);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt2!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
