package com.org.InsertDB;

import java.sql.*;
import java.util.Properties;

public class Insert {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:mysql://localhost:3306";
		Statement stmt=null;
		
		String  insqry="insert int oejm5.emp values(1,'sufiya',90000.00,Google)";
		Properties info=new Properties();
		info.put("user", "root");
		info.put("password","root" );
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded and registered");
			
			con=DriverManager.getConnection(url, info);
			System.out.println("connection established");
			
			stmt=con.createStatement();
			System.out.println("platform created");
			
			
			stmt.executeUpdate(insqry);
			
			System.out.println("data inserted successfully...");
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
				e.printStackTrace();
				}
			}
			if(stmt!=null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
