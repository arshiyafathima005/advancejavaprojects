package com.org.FetchData;

import java.sql.*;
import java.util.Scanner;

public class FetchAbsolute {

	public static void main(String[] args) {
		Connection con=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		String qry="select * from oejm5.emp";
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the number of record u want to fetch");
		int n=sc.nextInt();
		sc.close();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("driver loaded and reg....");
			con=DriverManager.getConnection(url);
			System.out.println("connection established");
			pstmt=con.prepareStatement(qry);//compile
			System.out.println("platform created");
			rs=pstmt.executeQuery();//execute
			if(rs.absolute(n))
			{
				int id=rs.getInt(1);
				String name=rs.getString("name");
				double salary=rs.getDouble("salary");
				String company=rs.getString(4);
				System.out.println("employee details: "+id+" "+name+" "+salary+" "+company);
			}
			else
			{
				System.out.println("record not found");
			}
			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt!=null)
			{
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(rs!=null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
