package com.org.FetchData;
import java.sql.*;
public class FetchApp {
public static void main(String[] args) {
	Connection con=null;
	String url="jdbc:mysql://localhost:3306?user=root&password=root";
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String qry="select * from oejm5.emp";
try {
	Class.forName("com.mysql.jdbc.Driver");
	con=DriverManager.getConnection(url);
	pstmt=con.prepareStatement(qry);
	rs=pstmt.executeQuery();
   if(rs.next())//while for fetching all data in record/fetch multiple records
	{
		int id=rs.getInt("id");
		String name=rs.getString(2);
		double salary=rs.getDouble("salary");
		String company=rs.getString(4);
		System.out.println("employee details:"+id+" "+name+" "+salary+" "+company);
	}
	
} catch (ClassNotFoundException | SQLException e) {
	e.printStackTrace();
}
finally
{
	if(con!=null) {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	if(pstmt!=null) {
		try {
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	if(rs!=null) {
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		}
}
}
