package com.org.servletLifeCycle;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;

public class FirstServlet extends GenericServlet 
{
	public FirstServlet()
	{
		System.out.println("servlet object is created");
	}
	 @Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("servlet object is initialized");
	}
	@Override
	public void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException
	{
		String name=req.getParameter("nm");
		String palce=req.getParameter("pl");
		PrintWriter out=resp.getWriter();
		out.println("<html><body bgcolor='cyan'><font color='blue' size='20'>welcome "
		+name+" to "+palce+"</font></body></html>");
		out.flush();
		out.close();
		
	}
	@Override
	public void destroy() {
		System.out.println("costly resourse are closed");
	}

}
