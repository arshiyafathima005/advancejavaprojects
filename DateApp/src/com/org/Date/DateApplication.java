package com.org.Date;

import java.sql.*;

public class DateApplication {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		PreparedStatement pstmt=null;
		String qry="insert into oejm5.date values(?)";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(url);
			pstmt=con.prepareStatement(qry);
			pstmt.setDate(1, new Date(1561278697000L));
			pstmt.executeUpdate();
			System.out.println("date inserted");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}

