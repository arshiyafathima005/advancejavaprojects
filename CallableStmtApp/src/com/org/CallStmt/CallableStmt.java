package com.org.CallStmt;

import java.sql.*;
import java.util.Scanner;

public class CallableStmt {

	public static void main(String[] args) {
		Connection con=null;
		String url="jdbc:mysql://localhost:3306?user=root&password=root";
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String qry="{call oejm5.empProc(?)}";
		Scanner sc=new Scanner(System.in);
		System.out.println("enter id");
		int id=sc.nextInt();	
		sc.close();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(url);
			cstmt=con.prepareCall(qry);
			cstmt.setInt(1, id);
			rs=cstmt.executeQuery();
			if(rs.next())
			{
				String name=rs.getString(1);
				System.out.println("welcome "+name);
			}
			else
			{
				System.err.println("id is invalid");
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
			{
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(cstmt!=null)
			{
				try {
					cstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(rs!=null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
